package work.andrefernandes.model;

public enum EventType {

	ADD, DELETE, UPDATE;
}
