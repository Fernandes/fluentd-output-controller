package work.andrefernandes.model;

import io.fabric8.kubernetes.api.model.Namespaced;
import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.model.annotation.Group;
import io.fabric8.kubernetes.model.annotation.Plural;
import io.fabric8.kubernetes.model.annotation.Version;

@Version("v1alpha1")
@Group("andrefernandes.fluentd.io")
@Plural("fluentdoutputs")
public class FluentdOutput extends CustomResource<FluentdOutputSpec, FluentdOutputStatus> implements Namespaced { 

	
	
}
