package work.andrefernandes.model;

public class FluentdOutputEvent {

	private String name;
	private EventType event;
	public FluentdOutputEvent(String name, EventType event) {
		super();
		this.name = name;
		this.event = event;
	}
	public String getName() {
		return name;
	}
	public EventType getEvent() {
		return event;
	}
	
	
}
