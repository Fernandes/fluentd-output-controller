package work.andrefernandes.fluentd;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.jboss.logging.Logger;

public class FluentdOutputCreator {

	private static final Logger LOG = Logger.getLogger(FluentdOutputCreator.class);
	
	private static final String FILE_PATTERN = "<match %s.**>\n"
			+ "    output xxxxx\n"
			+ "</match>";
	
	public static void main(String[] args) throws IOException {
		FluentdOutputCreator foc = new FluentdOutputCreator();
		foc.createNewOutputFile("test");
	}
	
	public static String getConfigMapNameFromOutputName(String outputName) {
		return  outputName + "-output";
	}
	
	public static String getFileNameFromOutputName(String outputName) {
		return  outputName + "-output.conf";
	}
	
	public void createNewOutputFile(String outputName) throws IOException {
		String fileName = getFileNameFromOutputName(outputName);
		
		LOG.infof("Creating new file with name: %s", fileName);
		
		FileWriter fileWriter = new FileWriter(fileName);
	    PrintWriter printWriter = new PrintWriter(fileWriter);
	    
	    printWriter.printf(FILE_PATTERN, outputName);
	    printWriter.close();
	}
}
