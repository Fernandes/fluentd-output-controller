package work.andrefernandes.controller;

import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.jboss.logging.Logger;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.client.informers.ResourceEventHandler;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.fabric8.kubernetes.client.informers.cache.Cache;
import io.fabric8.kubernetes.client.informers.cache.Lister;
import io.quarkus.runtime.QuarkusApplication;
import work.andrefernandes.fluentd.FluentdMainConfigurer;
import work.andrefernandes.model.EventType;
import work.andrefernandes.model.FluentdOutput;
import work.andrefernandes.model.FluentdOutputEvent;
import work.andrefernandes.model.FluentdOutputList;


public class FluentdOutputController  {

	private static final Logger LOG = Logger.getLogger(FluentdOutputController.class);
	
    private final SharedIndexInformer<FluentdOutput> fluentdOutputInformer;
    private final Lister<FluentdOutput> fluentdOutputLister;
    private final KubernetesClient kubernetesClient;
    private final MixedOperation<FluentdOutput, FluentdOutputList, Resource<FluentdOutput>> fluentdOutputClient;
	private final String namespace;
	private final BlockingQueue<FluentdOutputEvent> workqueue;
	private final MixedOperation<FluentdOutput, FluentdOutputList, Resource<FluentdOutput>> foClient;
	
	public FluentdOutputController(SharedIndexInformer<FluentdOutput> fluentdOutputInformer,
			KubernetesClient kubernetesClient,
			MixedOperation<FluentdOutput, FluentdOutputList, Resource<FluentdOutput>> foClient,
			String namespace) {
		
		this.foClient = foClient;
		this.fluentdOutputInformer = fluentdOutputInformer;
		this.fluentdOutputLister = new Lister<>(fluentdOutputInformer.getIndexer(), namespace);
		this.namespace = namespace;
		this.kubernetesClient = kubernetesClient;
		this.fluentdOutputClient = foClient;
		this.workqueue = new ArrayBlockingQueue<>(1024);
	}

	public void create() {
        // Set up an event handler for when Foo resources change
		fluentdOutputInformer.addEventHandler(new ResourceEventHandler<FluentdOutput>() {
            @Override
            public void onAdd(FluentdOutput fo) {
                enqueueFluentdOutput(fo, EventType.ADD);
            }

            @Override
            public void onUpdate(FluentdOutput fo, FluentdOutput newFoo) {
                enqueueFluentdOutput(newFoo, EventType.UPDATE);
            }

            @Override
            public void onDelete(FluentdOutput fo, boolean b) {
            	enqueueFluentdOutput(fo, EventType.DELETE);
            }
        });
    }
	
	private void enqueueFluentdOutput(FluentdOutput fo, EventType event) {
        LOG.infof("enqueueFoo(%s)", fo.getMetadata().getName());
        String key = Cache.metaNamespaceKeyFunc(fo);
        LOG.infof("Going to enqueue key %s", key);
        if (key != null && !key.isEmpty()) {
            LOG.info("Adding item to workqueue");
            workqueue.add(new FluentdOutputEvent(key, event));
        }
    }
	
	
	public void run() {
		LOG.infof("Starting %s controller", FluentdOutputController.class.getSimpleName());
        LOG.info("Waiting for informer caches to sync...");
        
        FluentdMainConfigurer fmc = new FluentdMainConfigurer(kubernetesClient);
        
        while (!Thread.currentThread().isInterrupted()) {
            try {
                LOG.info("trying to fetch item from workqueue...");
                
                if (workqueue.isEmpty()) {
                    LOG.info("Work Queue is empty");
                }
//                String key = workqueue.poll(3, TimeUnit.SECONDS);
                FluentdOutputEvent foEvent = workqueue.take();
                String key = foEvent.getName();
                Objects.requireNonNull(key, "key can't be null");
                LOG.infof("Got %s", key);
                if (key.isEmpty() || (!key.contains("/"))) {
                	LOG.warnf("invalid resource key: %s", key);
                }
                String name = key.split("/")[1];
//                FluentdOutputList fol = foClient.list();
                
                FluentdOutput fo = fluentdOutputLister.get(key.split("/")[1]);
                if (fo == null) {
                    LOG.errorf("FluentdOutput %s in workqueue no longer exists", name);
                    continue;
                }
                switch(foEvent.getEvent()) {
				case ADD:
					fmc.createOutput(fo);
					break;
				case DELETE:
					fmc.deleteOutput(fo);
					break;
				case UPDATE:
					break;
				default:
					LOG.fatal("Event not implemented!!");
					break;
                
                }

            } catch (InterruptedException interruptedException) {
                Thread.currentThread().interrupt();
                LOG.error("controller interrupted..");
            }
        }
    }

//	@Override
//	public int run(String... args) throws Exception {
//		// TODO Auto-generated method stub
//		return 0;
//	}
	
}
