package work.andrefernandes;

import org.jboss.logging.Logger;

import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.fabric8.kubernetes.client.informers.SharedInformerFactory;
import io.quarkus.runtime.annotations.QuarkusMain;
import work.andrefernandes.controller.FluentdOutputController;
import work.andrefernandes.model.FluentdOutput;
import work.andrefernandes.model.FluentdOutputList;

@QuarkusMain
public class FluentdOutputControllerMain  {

	private static final Logger LOG = Logger.getLogger(FluentdOutputControllerMain.class);


	public static void main(String... args) {
		try (KubernetesClient client = new DefaultKubernetesClient()) {
            String namespace = client.getNamespace();
            if (namespace == null) {
                LOG.info("No namespace found via config, assuming fluentd.");
                namespace = "default";
            }

            LOG.infof("Using namespace : %s", namespace);

            SharedInformerFactory informerFactory = client.informers();

            MixedOperation<FluentdOutput, FluentdOutputList, Resource<FluentdOutput>> foClient = client.customResources(FluentdOutput.class, FluentdOutputList.class);
            SharedIndexInformer<FluentdOutput> foSharedIndexInformer = informerFactory.sharedIndexInformerForCustomResource(FluentdOutput.class, FluentdOutputList.class, 10 * 60 * 1000);
            
            
            FluentdOutputController controller = new FluentdOutputController(foSharedIndexInformer, client, foClient, namespace);

            controller.create();
            informerFactory.startAllRegisteredInformers();
            informerFactory.addSharedInformerEventListener(exception -> LOG.error("Exception occurred, but caught", exception));

            LOG.info("Starting FluentdOutput Controller");
            controller.run();
        } catch (KubernetesClientException exception) {
            LOG.error("Kubernetes Client Exception : ", exception);
        }
	}

}
